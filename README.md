# Treasure Hunt README

Found on:
- https://bitbucket.org/antoine_lilin/vue-treasurehunt-usrs04/src/master/
- https://hopeful-brahmagupta-8fd8cf.netlify.app/

## Requirements

- NFC : read & write
- Localisation

## How to use

On load the page will ask for it's geolocation to check if there are any treasure in the user's vicinity. If there are and alert will pop on screen to warn the user and give them an approximate distance.

You can write a treasure's NFC code on a chip by pressing the corresponding item.

By pressing "Read NFC" button you can make the app ready to read an NFC chip and check if it's id matches one of your treasures.

By pressing "Scan" you can refresh your geolocation. Refreshing geolocation will automatically check for nearby tokens.

## Missing features

Crosschecking token's supposed coordinates with user's current coordinates.

Add modal to warn user of low accuracy geolocation readings / absence of NFC capabilities / absence of Geolocation capabilities

Needs to be connected to a backend to be able to create, publish, save and save user progress on treasure hunts.

PWA capabilities need to be added.

## Polish

Transition between reading and writing on an NFC chip needs some polish.

Need more user feedback when starting an action.

# Original Vue README treasure-hunt

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
